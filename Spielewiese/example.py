import math
import readline

def abc_generator():
    yield("a")
    yield("b")
    yield("c")
    yield("d")

def fak(n):
    if(n<=1):
        return 1
    else:
        return n*fak(n-1)

def fibonacci(depth):
    if(depth==1 or depth==2):
        return 1
    else:
        return fibonacci(depth-1)+fibonacci(depth-2)

#x = abc_generator()
#for f in x:
#    print f,
#print

#print(fak(3))
x=0
var = input("please give a length for the fibonacci numbers: ")
#xstr = readline.get_line_buffer()
x = int(var)

for i in range(1,x+1):
    print(fibonacci(i)),
print