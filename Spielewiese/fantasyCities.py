def getFantasyWeather(city : str):
	if(city is 'Mordor'):
		output_String = 'Now the current weather of ' + city + '\n-------------------------\n\nThe weather is sulfur rain. The temperature is 67.31°C and the wind speed is 0.4km/h.'
	elif(city is 'Auenland'):
		output_String = 'Now the current weather of ' + city + '\n-------------------------\n\nThe weather is sunny. The temperature is 21.23°C and the wind speed is 2.4km/h.'
	else:
		output_String = 'NIX'
	return output_String
