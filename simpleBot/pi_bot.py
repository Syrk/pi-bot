import wetter
import datetime
import temperature
import discord
from discord.ext import commands
import fantasyCities

TOKEN = 'Mzc5MzYxMDE0MzQ2NDE2MTM4.DOo7AQ.RLJ1-JU2q3d8E4J9NBjG6By-Li0'
description = '''Pi-Bot in Python'''
bot = commands.Bot(command_prefix='//', description=description)

@bot.event
async def on_ready():
    print('logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('{:%d.%m.%Y %H:%M:%S}'.format(datetime.datetime.now()))
    print('------')

#@bot.event
#async def on_message(message):
#    if message.content.startswith('$thumb'):
#        channel = message.channel
#        await channel.send('Send me that 👍 reaction, mate')
#
#        def check(reaction, user):
#            return user == message.author and str(reaction.emoji) == '👍'
#
#        try:
#            reaction, user = await client.wait_for('reaction_add', timeout=60.0, check=check)
#        except asyncio.TimeoutError:
#            await channel.send('👎')
#        else:
#            await channel.send('👍')


@bot.command()
async def hello():
    """Says World"""
    await bot.say("world")

@bot.command()
async def add(left : int, right : int):
    """Adds two numbers together."""
    await bot.say(left + right)

@bot.command()
async def marco():
    """Ping pong with marco polo"""
    await bot.say("polo")

@bot.command()
async def temp():
    """Give the temperature of the pi."""
    await bot.say(temperature.getCPUtemperature())

@bot.command()
async def weather(city : str):
    """Give the current weather of the city"""
    await bot.say(wetter.getWeather(city))

bot.run(TOKEN)
