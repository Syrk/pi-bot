import json
import requests
import re
import datetime
import fantasyCities

def getWeather(city : str):
	fantasy_test = fantasyCities.getFantasyWeather(city)
	print(fantasy_test)
	if(fantasy_test is 'NIX'):
		api_string = 'http://api.openweathermap.org/data/2.5/weather?q={CITY}&APPID=e7047929c6f62455bfe0f6d72b904ef2'

		api_replaced = api_string.replace("{CITY}",city)

		weather_dict = (requests.get(api_replaced)).json()


		temp = float("{0:.2f}".format(float((weather_dict['main'])['temp'])-273.15))
		wind_speed = float("{0:.2f}".format(float((weather_dict['wind'])['speed']))) 
		humidity = float("{0:.2f}".format(float((weather_dict['main'])['humidity'])))
		cloud = float("{0:.2f}".format(float((weather_dict['clouds'])['all'])))

		weather_kind = ((weather_dict['weather'])[0])['description']

		sunrise = int((weather_dict['sys'])['sunrise'])
		sunset = int((weather_dict['sys'])['sunset'])
		date = int(weather_dict['dt'])

		Output_String = "Now the current weather of {CITY}\n-------------------------\n\nThe weather is {WEATHER_KIND}. The temperature is {TEMPERATUR}°C and the wind speed is {WIND}km/h."

		Output_String = Output_String.replace("{CITY}",city)
		Output_String = Output_String.replace("{TEMPERATUR}",str(temp))
		Output_String = Output_String.replace("{WIND}",str(wind_speed))
		Output_String = Output_String.replace("{WEATHER_KIND}",weather_kind)

		return(Output_String)
	else:
		return (fantasy_test)


