import datetime 
import os.path


def write(logtext : str):
    lognumber = 1
    
    filename = "logfile"
    if(os.path.isfile(filename)):
        file_object = open(filename,"r")
        for line in file_object:
            pass
        lognumber = int(line) + 1
        print(lognumber)
    
        file_object.close()
    
    
    file_object = open("logfile","a")
    
    now = datetime.datetime.now()
    file_object.write("\nDate: " + str(now) + "\n")
    file_object.write("-----------------------\n")
    file_object.write(logtext)
    file_object.write("-----------------------\n\n\n")
    file_object.write(repr(lognumber))
    
    file_object.close()